// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeSpeed1.h"
#include "SnakeBase.h"

// Sets default values
ASnakeSpeed1::ASnakeSpeed1()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeSpeed1::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeSpeed1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeSpeed1::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SnakeSpeedTwo();

			Destroy();
		}

	}
}

